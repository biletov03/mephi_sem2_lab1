#include <stdio.h>
#include <malloc.h>
#include "hadr.h"

int main() {
    enum Tipe tipe;
    enum Do manu;
    void * vect_1 = NULL;
    void * vect_2 = NULL;
    void * vect_res = NULL;
    do{
        printf("What do you want:\n1) Natural\n2) Real\n3) Complex\n4) Int test\n5) Float test\n6) Complex test\n7) Exit\n");
        scanf("%d", &tipe);
        switch(tipe) {
            case Natural:

                do {
                    printf("What do you want to make:\n1) Input_vector_1\n2) Input_vector_2\n3) Print first\n4) Print second\n5) Print result\n6) Sum\n7) Scalar product\n8) Vector product\n9) Back\n");
                    scanf("%d", &manu);

                    switch (manu) {
                        case Input_vector_1:
                        {
                            int *array = malloc(sizeof(int) * 3);
                            printf("Input coordinates\n");
                            scanf("%d %d %d", array, array + 1, array + 2);
                            vect_1 = input_int(vect_1,array);
                            free(array);
                        }
                            break;
                        case Input_vector_2:
                        {
                            int *array = malloc(sizeof(int) * 3);
                            printf("Input coordinates\n");
                            scanf("%d %d %d", array, array + 1, array + 2);
                            vect_2 = input_int(vect_2,array);
                            free(array);
                        }
                            break;
                        case Print_1:
                            printvector_int(vect_1);
                            break;
                        case Print_2:
                            printvector_int(vect_2);
                            break;
                        case Print_res:
                            printvector_int(vect_res);
                            break;
                        case Sum:
                            vect_res = clear_int(vect_res);

                            vect_res = sum_in(vect_1, vect_2);
                            break;
                        case Scalarpr:
                            scalarpr_in(vect_1, vect_2);
                            break;
                        case Vectorpr:
                            vect_res = clear_int(vect_res);

                            vect_res = vectorpr_in(vect_1, vect_2);
                            break;
                        case Back:
                            vect_1 = clear_int(vect_1);
                            vect_2 = clear_int(vect_2);
                            vect_res = clear_int(vect_res);
                            break;
                        default:
                            printf("Repid input\n");
                            break;
                    }
                } while(manu != Back);

                break;
            case Real:
                do {
                    printf("What do you want to make:\n1) Input_vector_1\n2) Input_vector_2\n3) Print first\n4) Print second\n5) Print result\n6) Sum\n7) Scalar product\n8) Vector product\n9) Back\n");
                    scanf("%d", &manu);

                    switch (manu) {
                        case Input_vector_1:
                        {
                            float *array = malloc(sizeof(float) * 3);
                            printf("Input coordinates\n");
                            scanf("%f %f %f", array, array + 1, array + 2);
                            vect_1 = input_float(vect_1,array);
                            free(array);
                        }
                            break;
                        case Input_vector_2:
                        {
                            float *array = malloc(sizeof(float) * 3);
                            printf("Input coordinates\n");
                            scanf("%f %f %f", array, array + 1, array + 2);
                            vect_2 = input_float(vect_2,array);
                            free(array);
                        }
                            break;
                        case Print_1:
                            printvector_float(vect_1);
                            break;
                        case Print_2:
                            printvector_float(vect_2);
                            break;
                        case Print_res:
                            printvector_float(vect_res);
                            break;
                        case Sum:
                            vect_res = clear_float(vect_res);

                            vect_res = sum_in(vect_1, vect_2);
                            break;
                        case Scalarpr:
                            scalarpr_in(vect_1, vect_2);
                            break;
                        case Vectorpr:
                            vect_res = clear_float(vect_res);

                            vect_res = vectorpr_in(vect_1, vect_2);
                            break;
                        case Back:
                            vect_1 = clear_float(vect_1);
                            vect_2 = clear_float(vect_2);
                            vect_res = clear_float(vect_res);
                            break;
                        default:
                            printf("Repid input\n");
                            break;
                    }
                } while(manu != Back);

                break;
            case Complex:
                do {
                    printf("What do you want to make:\n1) Input_vector_1\n2) Input_vector_2\n3) Print first\n4) Print second\n5) Print result\n6) Sum\n7) Scalar product\n8) Vector product\n9) Back\n");
                    scanf("%d", &manu);

                    switch (manu) {
                        case Input_vector_1:
                        {
                            float *array = malloc(sizeof(float) * 6);
                            printf("Input coordinates for Re and Im parts\n");
                            scanf("%f %f %f %f %f %f", array, array + 1, array + 2, array + 3, array + 4, array + 5);
                            vect_1 = input_complex(vect_1, array);
                            free(array);
                        }
                            break;
                        case Input_vector_2:
                        {
                            float *array = malloc(sizeof(float) * 6);
                            printf("Input coordinates for Re and Im parts\n");
                            scanf("%f %f %f %f %f %f", array, array + 1, array + 2, array + 3, array + 4, array + 5);
                            vect_2 = input_complex(vect_2, array);
                            free(array);
                        }
                            break;
                        case Print_1:
                            printvector_complex(vect_1);
                            break;
                        case Print_2:
                            printvector_complex(vect_2);
                            break;
                        case Print_res:
                            printvector_complex(vect_res);
                            break;
                        case Sum:
                            vect_res = clear_complex(vect_res);

                            vect_res = sum_in(vect_1, vect_2);
                            break;
                        case Scalarpr:
                            scalarpr_in(vect_1, vect_2);
                            break;
                        case Vectorpr:
                            vect_res = clear_complex(vect_res);

                            vect_res = vectorpr_in(vect_1, vect_2);
                            break;
                        case Back:
                            vect_1 = clear_complex(vect_1);
                            vect_2 = clear_complex(vect_2);
                            vect_res = clear_complex(vect_res);
                            break;
                        default:
                            printf("Repid input\n");
                            break;
                    }
                } while(manu != Back);

                break;
            case Int_test:
            {
                int cord_1[] = {1, 2, 3};
                int cord_2[] = {3, 2, 1};

                vect_1 = input_int(vect_1, cord_1);

                printf("All function then vector_2 is empty\n");
                vect_res = sum_in(vect_1, vect_2);
                vect_res = vectorpr_in(vect_1, vect_2);
                scalarpr_in(vect_1, vect_2);

                vect_2 = input_int(vect_2, cord_2);


                vect_res = sum_in(vect_1, vect_2);
                printf("First vector:\n");
                printvector_int(vect_1);
                printf("Second vector:\n");
                printvector_int(vect_2);
                printf("Result vector(sum):\n");
                printvector_int(vect_res);
                printf("Result vector(vector product):\n");
                vect_res = vectorpr_in(vect_1, vect_2);
                printvector_int(vect_res);
                printf("Result vector(scal product):\n");
                scalarpr_in(vect_1, vect_2);

                clear_int(vect_1);
                clear_int(vect_2);
                clear_int(vect_res);
            }
                break;
            case Float_test:
            {
                float cord_1[] = {1, 2, 3};
                float cord_2[] = {3, 2, 1};

                vect_1 = input_float(vect_1, cord_1);

                printf("All function then vector_2 is empty\n");
                vect_res = sum_in(vect_1, vect_2);
                vect_res = vectorpr_in(vect_1, vect_2);
                scalarpr_in(vect_1, vect_2);

                vect_2 = input_float(vect_2, cord_2);


                vect_res = sum_in(vect_1, vect_2);
                printf("First vector:\n");
                printvector_float(vect_1);
                printf("Second vector:\n");
                printvector_float(vect_2);
                printf("Result vector(sum):\n");
                printvector_float(vect_res);
                printf("Result vector(vector product):\n");
                vect_res = vectorpr_in(vect_1, vect_2);
                printvector_float(vect_res);
                printf("Result vector(scal product):\n");
                scalarpr_in(vect_1, vect_2);

                clear_float(vect_1);
                clear_float(vect_2);
                clear_float(vect_res);
            }
                break;
            case Complex_test:
            {
                float cord_1[] = {1, 2, 3, 1, 2, 3};
                float cord_2[] = {3, 2, 1, 3, 2, 1};

                vect_1 = input_complex(vect_1, cord_1);

                printf("All function then vector_2 is empty\n");
                vect_res = sum_in(vect_1, vect_2);
                vect_res = vectorpr_in(vect_1, vect_2);
                scalarpr_in(vect_1, vect_2);

                vect_2 = input_complex(vect_2, cord_2);


                vect_res = sum_in(vect_1, vect_2);
                printf("First vector:\n");
                printvector_complex(vect_1);
                printf("Second vector:\n");
                printvector_complex(vect_2);
                printf("Result vector(sum):\n");
                printvector_complex(vect_res);
                printf("Result vector(vector product):\n");
                vect_res = vectorpr_in(vect_1, vect_2);
                printvector_complex(vect_res);
                printf("Result vector(scal product):\n");
                scalarpr_in(vect_1, vect_2);

                clear_complex(vect_1);
                clear_complex(vect_2);
                clear_complex(vect_res);
            }
                break;
            case Exit:
                break;
            default:
                printf("Repid input\n");
                break;
        }
    } while(tipe != Exit);
    return 0;
}
