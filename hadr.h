struct Info{
    void * (* scalpr)(void *, void *);
    void * (* sum)(void *, void *);
    void * (* vectpr)(void *, void *);
};

struct Vector {
    struct Info * info;
    void * x;
    void * y;
    void * z;
};

void * input_int(struct Vector * vect, int * cord);

void printvector_int(struct Vector * vect);

void * clear_int(struct Vector * vect);

void * sum_int(struct Vector * vect_1, struct Vector * vect_2);

void scalarpr_int(struct Vector * vect_1, struct Vector * vect_2);

void * vectorpr_int(struct Vector * vect_1, struct Vector * vect_2);

void * create_vector_int();

void * create_info_int();

void * input_float(struct Vector * vect, float * cord);

void printvector_float(struct Vector * vect);

void * clear_float(struct Vector * vect);

void * sum_float(struct Vector * vect_1, struct Vector * vect_2);

void scalarpr_float(struct Vector * vect_1, struct Vector * vect_2);

void * vectorpr_float(struct Vector * vect_1, struct Vector * vect_2);

void * create_vector_float();

void * create_info_float();

void * input_complex(struct Vector * vect, float * cord);

void printvector_complex(struct Vector * vect);

void * clear_complex(struct Vector * vect);

void * sum_complex(struct Vector * vect_1, struct Vector * vect_2);

void scalarpr_complex(struct Vector * vect_1, struct Vector * vect_2);

void * vectorpr_complex(struct Vector * vect_1, struct Vector * vect_2);

void * create_vector_complex();

void * create_info_complex();

void * sum_in(struct Vector * vect_1, struct Vector * vect_2);

void scalarpr_in(struct Vector * vect_1, struct Vector * vect_2);

void * vectorpr_in(struct Vector * vect_1, struct Vector * vect_2);



enum Tipe {
    Natural = 1,
    Real,
    Complex,
    Int_test,
    Float_test,
    Complex_test,
    Exit
};

enum Do {
    Input_vector_1 = 1,
    Input_vector_2 = 2,
    Print_1,
    Print_2,
    Print_res,
    Sum,
    Scalarpr,
    Vectorpr,
    Back

};
