
#include <stdio.h>
#include <malloc.h>
#include "hadr.h"


void printvector_int(struct Vector * vect) {
    if (vect != NULL) {
        printf("x = %d\ny = %d\nz = %d\n", *(int*)(vect->x), *(int*)(vect->y), *(int*)(vect->z));
    } else {
        printf("Struct is empty\n");
    }
}

void * clear_int(struct Vector * vect) {
    if (vect != NULL) {
        int *x = vect->x;
        int *y = vect->y;
        int *z = vect->z;
        if ((vect->info) != NULL) {
            free(vect->info);
        }
        free(x);
        free(y);
        free(z);
        free(vect);
        return NULL;
    }
    return vect;
}

void * sum_int(struct Vector * vect_1, struct Vector * vect_2) {
    if ((vect_1) != NULL && (vect_2) != NULL) {
        struct Vector * vect_res = create_vector_int();
        *(int *) (vect_res->x) = *(int *) (vect_1->x) + *(int *) (vect_2->x);
        *(int *) (vect_res->y) = *(int *) (vect_1->y) + *(int *) (vect_2->y);
        *(int *) (vect_res->z) = *(int *) (vect_1->z) + *(int *) (vect_2->z);

        return vect_res;
    } else {
        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }
        return NULL;
    }
}

void scalarpr_int(struct Vector * vect_1, struct Vector * vect_2) {
    int res = 0;
    if ((vect_1) != NULL && (vect_2) != NULL) {
         res = (*(int *)(vect_1->x)) * (*(int *) (vect_2->x)) + (*(int *) (vect_1->y)) * (*(int *) (vect_2->y)) + (*(int *) (vect_1->z)) * (*(int *) (vect_2->z));
        printf("Scalar product = %d\n", res);
    } else {
        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }

    }
}

void * vectorpr_int(struct Vector * vect_1, struct Vector * vect_2) {

    if ((vect_1) != NULL && (vect_2) != NULL) {
        struct Vector * vect_res = create_vector_int();
        *(int *) (vect_res->x) = *(int *) (vect_1->y) * *(int *) (vect_2->z) - *(int *) (vect_1->z) * *(int *) (vect_2->y);
        *(int *) (vect_res->y) = *(int *) (vect_1->z) * *(int *) (vect_2->x) - *(int *) (vect_1->x) * *(int *) (vect_2->z);
        *(int *) (vect_res->z) = *(int *) (vect_1->x) * *(int *) (vect_2->y) - *(int *) (vect_1->y) * *(int *) (vect_2->x);
        return vect_res;
    } else {

        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }
        return NULL;
    }
}

void * create_vector_int() {
    void * vect = malloc(sizeof(struct Vector));

    ((struct Vector *)vect)->info = NULL;
    ((struct Vector *)vect)->x = malloc(sizeof(int));
    ((struct Vector *)vect)->y = malloc(sizeof(int));
    ((struct Vector *)vect)->z = malloc(sizeof(int));
    return vect;
}

void * create_info_int() {
    void * Info = malloc(sizeof(struct Info));

    ((struct Info *)Info)->sum = (void *)sum_int;
    ((struct Info *)Info)->scalpr = (void *)scalarpr_int;
    ((struct Info *)Info)->vectpr = (void *)vectorpr_int;

    return Info;
}

void * input_int(struct Vector * vect, int * cord) {
    if (vect == NULL) {
        vect = create_vector_int();
        ((struct Vector * )vect)->info = create_info_int();
    }
    ((int *)(vect->x))[0] = cord[0];
    ((int *)(vect->y))[0] = cord[1];
    ((int *)(vect->z))[0] = cord[2];
    return vect;
}

void printvector_float(struct Vector * vect) {
    if (vect != NULL) {
        printf("x = %.2f\ny = %.2f\nz = %.2f\n", *(float*)(vect->x), *(float*)(vect->y), *(float*)(vect->z));
    } else {
        printf("Struct is empty\n");
    }
}

void * clear_float(struct Vector * vect) {
    if (vect != NULL) {
        float *x = vect->x;
        float *y = vect->y;
        float *z = vect->z;
        if ((vect->info) != NULL) {
            free(vect->info);
        }
        free(x);
        free(y);
        free(z);
        free(vect);
        return NULL;
    }
    return vect;
}

void * sum_float(struct Vector * vect_1, struct Vector * vect_2) {
    if ((vect_1) != NULL && (vect_2) != NULL) {
        struct Vector * vect_res = create_vector_float();
        *(float *) (vect_res->x) = *(float *) (vect_1->x) + *(float *) (vect_2->x);
        *(float *) (vect_res->y) = *(float *) (vect_1->y) + *(float *) (vect_2->y);
        *(float *) (vect_res->z) = *(float *) (vect_1->z) + *(float *) (vect_2->z);

        return vect_res;
    } else {
        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }
        return NULL;
    }
}

void scalarpr_float(struct Vector * vect_1, struct Vector * vect_2) {
    float res = 0;
    if ((vect_1) != NULL && (vect_2) != NULL) {
        res = (*(float *)(vect_1->x)) * (*(float *) (vect_2->x)) + (*(float *) (vect_1->y)) * (*(float *) (vect_2->y)) + (*(float *) (vect_1->z)) * (*(float *) (vect_2->z));
        printf("Scalar product = %.2f\n", res);
    } else {
        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }

    }
}

void * vectorpr_float(struct Vector * vect_1, struct Vector * vect_2) {

    if ((vect_1) != NULL && (vect_2) != NULL) {
        struct Vector * vect_res = create_vector_float();
        *(float *) (vect_res->x) = *(float *) (vect_1->y) * *(float *) (vect_2->z) - *(float *) (vect_1->z) * *(float *) (vect_2->y);
        *(float *) (vect_res->y) = *(float *) (vect_1->z) * *(float *) (vect_2->x) - *(float *) (vect_1->x) * *(float *) (vect_2->z);
        *(float *) (vect_res->z) = *(float *) (vect_1->x) * *(float *) (vect_2->y) - *(float *) (vect_1->y) * *(float *) (vect_2->x);
        return vect_res;
    } else {

        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }
        return NULL;
    }
}

void * create_vector_float() {
    void * vect = malloc(sizeof(struct Vector));

    ((struct Vector *)vect)->info = NULL;
    ((struct Vector *)vect)->x = malloc(sizeof(float));
    ((struct Vector *)vect)->y = malloc(sizeof(float));
    ((struct Vector *)vect)->z = malloc(sizeof(float));
    return vect;
}

void * create_info_float() {
    void * Info = malloc(sizeof(struct Info));

    ((struct Info *)Info)->sum = (void *)sum_float;
    ((struct Info *)Info)->scalpr = (void *)scalarpr_float;
    ((struct Info *)Info)->vectpr = (void *)vectorpr_float;

    return Info;
}

void * input_float(struct Vector * vect, float * cord) {
    if (vect == NULL) {
        vect = create_vector_float();
        ((struct Vector * )vect)->info = create_info_float();
    }
    ((float *)(vect->x))[0] = cord[0];
    ((float *)(vect->y))[0] = cord[1];
    ((float *)(vect->z))[0] = cord[2];
    return vect;
}

void printvector_complex(struct Vector * vect) {
    if (vect != NULL) {
        printf("x = %.2f + i * %.2f\ny = %.2f + i * %.2f\nz = %.2f + i * %.2f\n", ((float*)(vect->x))[0], ((float*)(vect->x))[1], ((float*)(vect->y))[0], ((float*)(vect->y))[1], ((float*)(vect->z))[0], ((float*)(vect->z))[1]);
    } else {
        printf("Struct is empty\n");
    }
}

void * clear_complex(struct Vector * vect) {
    if (vect != NULL) {
        float *x = vect->x;
        float *y = vect->y;
        float *z = vect->z;
        if ((vect->info) != NULL) {
            free(vect->info);
        }
        free(x);
        free(y);
        free(z);
        free(vect);
        return NULL;
    }
    return vect;
}

void * sum_complex(struct Vector * vect_1, struct Vector * vect_2) {
    if ((vect_1) != NULL && (vect_2) != NULL) {
        struct Vector * vect_res = create_vector_complex();
        *(float *) (vect_res->x) = *(float *) (vect_1->x) + *(float *) (vect_2->x);
        *(float *) (vect_res->y) = *(float *) (vect_1->y) + *(float *) (vect_2->y);
        *(float *) (vect_res->z) = *(float *) (vect_1->z) + *(float *) (vect_2->z);

        *((float *) (vect_res->x) + 1) = *((float *) (vect_1->x) + 1) + *((float *) (vect_2->x) + 1);
        *((float *) (vect_res->y) + 1) = *((float *) (vect_1->y) + 1) + *((float *) (vect_2->y) + 1);
        *((float *) (vect_res->z) + 1) = *((float *) (vect_1->z) + 1) + *((float *) (vect_2->z) + 1);

        return vect_res;
    } else {
        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }
        return NULL;
    }
}

void scalarpr_complex(struct Vector * vect_1, struct Vector * vect_2) {
    float res_Re = 0, res_Im = 0;
    if ((vect_1) != NULL && (vect_2) != NULL) {
        res_Re = (*(float *)(vect_1->x)) * (*(float *) (vect_2->x)) + (*(float *) (vect_1->y)) * (*(float *) (vect_2->y)) + (*(float *) (vect_1->z)) * (*(float *) (vect_2->z)) - (*((float *) (vect_1->x) + 1)) * (*((float *) (vect_2->x) + 1)) - (*((float *) (vect_1->y) + 1)) * (*((float *) (vect_2->y) + 1)) - (*((float *) (vect_1->z) + 1)) * (*((float *) (vect_2->z) + 1));

        res_Im = (*((float *) (vect_1->x))) * (*((float *) (vect_2->x) + 1)) + (*((float *) (vect_1->x) + 1)) * (*((float *) (vect_2->x))) + (*((float *) (vect_1->y))) * (*((float *) (vect_2->y) + 1)) + (*((float *) (vect_1->y) + 1)) * (*((float *) (vect_2->y))) + (*((float *) (vect_1->z))) * (*((float *) (vect_2->z) + 1)) + (*((float *) (vect_1->z) + 1)) * (*((float *) (vect_2->z)));
        printf("Scalar product = %.2f + i * %.2f\n", res_Re, res_Im);
    } else {
        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }

    }
}

void * vectorpr_complex(struct Vector * vect_1, struct Vector * vect_2) {

    if ((vect_1) != NULL && (vect_2) != NULL) {
        struct Vector * vect_res = create_vector_float();
        *(float *) (vect_res->x) = *(float *) (vect_1->y) * *(float *) (vect_2->z) - *((float *) (vect_1->y) + 1) * *((float *) (vect_2->z) + 1) - *(float *) (vect_1->z) * *(float *) (vect_2->y) + *((float *) (vect_1->z) + 1) * *((float *) (vect_2->y) + 1);
        *(float *) (vect_res->y) = *(float *) (vect_1->z) * *(float *) (vect_2->x) - *((float *) (vect_1->z) + 1) * *((float *) (vect_2->x) + 1) - *(float *) (vect_1->x) * *(float *) (vect_2->z) + *((float *) (vect_1->x) + 1) * *((float *) (vect_2->z) + 1);
        *(float *) (vect_res->z) = *(float *) (vect_1->x) * *(float *) (vect_2->y) - *((float *) (vect_1->x) + 1) * *((float *) (vect_2->y) + 1) - *(float *) (vect_1->y) * *(float *) (vect_2->x) + *((float *) (vect_1->y) + 1) * *((float *) (vect_2->x) + 1);

        *((float *) (vect_res->x) + 1) = *((float *) (vect_1->y) + 1) * *(float *) (vect_2->z) + *(float *) (vect_1->y) * *((float *) (vect_2->z) + 1) - *((float *) (vect_1->z) + 1) * *(float *) (vect_2->y) - *(float *) (vect_1->z) * *((float *) (vect_2->y) + 1);
        *((float *) (vect_res->y) + 1) = *((float *) (vect_1->z) + 1) * *(float *) (vect_2->x) + *(float *) (vect_1->z) * *((float *) (vect_2->x) + 1) - *((float *) (vect_1->x) + 1) * *(float *) (vect_2->z) - *(float *) (vect_1->x) * *((float *) (vect_2->z) + 1);
        *((float *) (vect_res->z) + 1) = *((float *) (vect_1->x) + 1) * *(float *) (vect_2->y) + *(float *) (vect_1->x) * *((float *) (vect_2->y) + 1) - *((float *) (vect_1->y) + 1) * *(float *) (vect_2->x) - *(float *) (vect_1->y) * *((float *) (vect_2->x) + 1);

        return vect_res;
    } else {

        if(vect_1 == NULL) {
            printf("Vector-1 is emty!\n");
        }
        if(vect_2 == NULL) {
            printf("Vector-2 is emty!\n");
        }
        return NULL;
    }
}

void * create_vector_complex() {
    void * vect = malloc(sizeof(struct Vector));

    ((struct Vector *)vect)->info = NULL;
    ((struct Vector *)vect)->x = malloc(sizeof(float) * 2);
    ((struct Vector *)vect)->y = malloc(sizeof(float) * 2);
    ((struct Vector *)vect)->z = malloc(sizeof(float) * 2);
    return vect;
}

void * create_info_complex() {
    void * Info = malloc(sizeof(struct Info));

    ((struct Info *)Info)->sum = (void *) sum_complex;
    ((struct Info *)Info)->scalpr = (void *) scalarpr_complex;
    ((struct Info *)Info)->vectpr = (void *) vectorpr_complex;

    return Info;
}

void * input_complex(struct Vector * vect, float * cord) {
    if (vect == NULL) {
        vect = create_vector_complex();
        ((struct Vector * )vect)->info = create_info_complex();
    }
    ((float *)(vect->x))[0] = cord[0];
    ((float *)(vect->y))[0] = cord[2];
    ((float *)(vect->z))[0] = cord[4];
    ((float *)(vect->x))[1] = cord[1];
    ((float *)(vect->y))[1] = cord[3];
    ((float *)(vect->z))[1] = cord[5];

    return vect;
}

void * sum_in(struct Vector * vect_1, struct Vector * vect_2) {
    return ((struct Info *)(vect_1->info))->sum(vect_1, vect_2);
}

void scalarpr_in(struct Vector * vect_1, struct Vector * vect_2) {
    ((struct Info *)(vect_1->info))->scalpr(vect_1, vect_2);
}

void * vectorpr_in(struct Vector * vect_1, struct Vector * vect_2) {
    return ((struct Info *)(vect_1->info))->vectpr(vect_1, vect_2);
}